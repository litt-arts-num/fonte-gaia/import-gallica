
<?php

$context  = stream_context_create(['http' => array('header' => 'Accept: application/xml')]);

$ark = $_POST["ark"];
$url = 'https://gallica.bnf.fr/SRU?operation=searchRetrieve&version=1.2&maximumRecords=10&startRecord=1&query=dc.identifier%20all%20'.$ark;

$xml = new \DOMDocument();
$xml->loadXml(file_get_contents($url, false, $context));


$elements = $xml->getElementsByTagName('dc');
//$header = [];
//$content =  [];
$datarray = []

/*
if (!is_null($elements)) {
  foreach ($elements as $element) {
    var_dump($element);
    var_dump("_________________");
    $nodes = $element->childNodes;
    foreach ($nodes as $node) {
      //var_dump($node);
      if ($node->localName) {
        array_push($header, $node->localName);
        array_push($content, $node->textContent);
      }
    }
  }
}

$list = [$header,$content];
*/
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">

  <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
  </head>

  <body>
    <pre>
      <?php

      if (!is_null($elements)) {
        foreach ($elements as $element) {
          $nodes = $element->childNodes;
          foreach ($nodes as $node) {
            if ($node->localName) {
            // $datarray[$node->localName] = $node->textContent;
            // $node->localName = KEY ;  $node->textContent; = VALUE
            // Mais si la KEY existe déjà, alors on concatène la nouvelle valeur à l'ancienne avec | comme séparateur
             if (isset($datarray[$node->localName])) {
               $newval = $datarray[$node->localName] . " | " . $node->textContent; //  nouvelle VALUE = ancienne Value + | + nouvelle VALUE
               $datarray[$node->localName] = $newval;
             }
             else {
               $datarray[$node->localName] = $node->textContent;
             }
            }
          }
        }
      }
    // https://gallica.bnf.fr/ark:/12148/btv1b106634195 : coupe au 23eme car.
    //echo (substr($ark, 23));
    $manifest = "https://gallica.bnf.fr/iiif/" . (substr($ark, 23)) . "/manifest.json";
    //echo $manifest ;
    $datarray["Has Format"] = $manifest;
//    var_dump($datarray);
    $colums_heads = [];
    $row_cells = [];
    // c'est idiot mais je suis obligé de recommencer poir utiliser fputcsv
    //echo "_______________________";
    //$fp = fopen('file.csv', 'w');
    foreach ($datarray as $k => $v) {
      array_push($colums_heads, ("Dublin Core:" . ucfirst($k)));
      array_push($row_cells, $v);
    }
    $tabelo = [$colums_heads, $row_cells];
    $fp = fopen('file.csv', 'w');
     foreach ($tabelo as $fields) {
       fputcsv($fp, $fields);
     }
       fclose($fp);
      ?>
    </pre>
    <div class="container">
      <div class="row">
        <div class="col">
          <a href="./file.csv">CSV</a>
        </div>
      </div>
    </div>
    <div>
      <center>
        <table>

      <?php
      // N'a pas encore de sens de rouvrir le même fichier ou un autre ...
      // Ferait mieux d'ECHO les valeurs ramenées, et de les rendre éditables avnat 1 push ...
      // d'un autre côté, donne envie d'avoir 1 table dans MYSQL ...   
        $file = fopen("/home/theo/Téléchargements/file(15).csv", "r");
        // Fetching data from csv file row by row
        while (($data = fgetcsv($file)) !== false) {
            // HTML tag for placing in row format
            echo "<tr>";
            foreach ($data as $i) {
                echo "<td>" . htmlspecialchars($i)
                    . "</td>";
            }
            echo "</tr> \n";
        }
        // Closing the file
        fclose($file);

        ?>
        </table>
      </center>
    </div>

  </body>



</html>
